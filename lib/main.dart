import 'package:flutter/material.dart';
import 'package:myproject/model/counter.dart';
import 'package:myproject/model/model.dart';
import 'package:myproject/my_app.dart';
import 'package:supabase_auth_ui/supabase_auth_ui.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Supabase.initialize(
    url: "https://jauleqpxusfufoamhkns.supabase.co",
    anonKey:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImphdWxlcXB4dXNmdWZvYW1oa25zIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTc0ODQ0NjksImV4cCI6MjAxMzA2MDQ2OX0.iyCcPkUE4i2fJ3fZ7xBujKLbC7Gb5ZUIvaE77hnOod0",
  );
  final supabase = Supabase.instance.client;

  supabase.auth.onAuthStateChange.listen((data) async {
    print("Auth state changed: ${data?.session?.user?.email}");
    final AuthChangeEvent event = data.event;
    if (supabase.auth.currentSession != null) {
      final userCounters = await supabase
          .from('user_counters')
          .select()
          .then((value) => value as List);
      addToCounters(userCounters);

      final myChannel = supabase.channel('my_channel');

      myChannel.on(
          RealtimeListenTypes.postgresChanges,
          ChannelFilter(
            event: '*',
            schema: 'public',
            table: 'user_counters',
          ), (payload, [ref]) {
        print("Payload:");
        print(payload);
        if (payload["eventType"] == "INSERT") {
          Model.modelInstance.counters
              .add(Counter(payload["new"]["counter"], payload["new"]["name"]));
        }
        if (payload["eventType"] == "UPDATE") {
          var name = payload["new"]["name"];
          var count = payload["new"]["counter"];
          Model.modelInstance.counters
              .firstWhere((element) => element.name == name)
              .counter
              .value = count as int;
        }
      }).subscribe();
    } else {
      Model.modelInstance.counters.clear();
    }
  });
  runApp(const MyApp());
}

void addToCounters(Iterable<dynamic> userCounters) {
  final counters = userCounters.map((cs) {
    return Counter(cs["counter"] as int, cs["name"] as String);
  });
  Model.modelInstance.counters.addAll(counters);
}
