import 'package:get/get.dart';
import 'package:myproject/model/counter.dart';

class Model {
  static final modelInstance = Model();

  final RxList<Counter> counters = RxList();
}
