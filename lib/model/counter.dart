import 'package:get/get.dart';

class Counter {
  late final RxInt counter;
  final String name;

  Counter(int initialCount, this.name) {
    counter = initialCount.obs;
  }

  incrementCounter() {
    counter.value = counter.value + 1;
  }
}
