import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myproject/model/counter.dart';
import 'package:myproject/model/model.dart';
import 'package:myproject/pages/login_page.dart';
import 'package:supabase_auth_ui/supabase_auth_ui.dart';

class MyHomePage extends StatelessWidget {
  MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    var newCounterName = TextEditingController();
    final supabase = Supabase.instance.client;
    return Scaffold(
      appBar: AppBar(
        title: Text(supabase.auth.currentUser!.email!),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: SizedBox(
            width: 800,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextField(
                  controller: newCounterName,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      onPressed: () async {
                        await supabase.from('user_counters').insert(
                            {'name': newCounterName.text, 'counter': 0});
                        newCounterName.clear();
                      },
                      child: const Text("Add counter.dart")),
                ),
                Obx(
                  () => Column(children: [
                    ...(Model.modelInstance.counters
                          ..sort(
                            (a, b) => a.name.compareTo(b.name),
                          ))
                        .map((counter) => CounterWidget(counter, supabase))
                  ]),
                ),
                OutlinedButton.icon(
                  icon: Icon(Icons.logout),
                  label: Text("Logout"),
                  onPressed: () {
                    supabase.auth
                        .signOut()
                        .then((value) => Get.offAll(LoginPage()));
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CounterWidget extends StatelessWidget {
  final SupabaseClient supabase;
  final Counter counter;

  const CounterWidget(
    this.counter,
    this.supabase, {
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Card(
        elevation: 6,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListTile(
            leading: Text(
              '${counter.counter.value}',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            title: Text(counter.name),
            trailing: Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: IconButton(
                  onPressed: () async {
                    await supabase.rpc("increment_user_counter",
                        params: {"p_name": counter.name});
                  },
                  icon: const Icon(Icons.add)),
            ),
          ),
        ),
      ),
    );
  }
}
