import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myproject/pages/my_home_page.dart';
import 'package:supabase_auth_ui/supabase_auth_ui.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SupaEmailAuth(
        onSignInComplete: (response) {
          Get.snackbar("Signed in!", "Now do your thing",
              snackPosition: SnackPosition.BOTTOM);
          Get.offAll(MyHomePage());
        },
        onSignUpComplete: (response) {
          Get.snackbar("Signed up!", "Verify email, and then login here.",
              snackPosition: SnackPosition.BOTTOM);
          // do something, for example: navigate("wait_for_email");
        },
        metadataFields: [],
      ),
    );
  }
}
