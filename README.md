# myproject

A new Flutter project.

## Frameworks and tools

- Libraries: https://pub.dev
- Handle flutter versions: https://fvm.app
- Dev environment: https://developer.android.com/studio
- State/observable/pages/navigation package: https://pub.dev/packages/get
- Theming: https://pub.dev/packages/flex_color_scheme/install
    - https://rydmike.com/flexcolorscheme/themesplayground-v7-2/#/
- Online database-and-more: https://supabase.com/
    - https://pub.dev/packages/supabase_flutter
    - https://pub.dev/packages/supabase_auth_ui

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
